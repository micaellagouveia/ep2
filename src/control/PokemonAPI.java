package control;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import model.Pokemon;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

public class PokemonAPI {
    List<Pokemon> random = new ArrayList();
    List<Pokemon> grass = new ArrayList();
    List<Pokemon> fire = new ArrayList();
    List<Pokemon> water = new ArrayList();
    List<Pokemon> bug = new ArrayList();
    List<Pokemon> normal = new ArrayList();
    List<Pokemon> poison = new ArrayList();
    List<Pokemon> electric = new ArrayList();
    List<Pokemon> ground  = new ArrayList();
    List<Pokemon> fighting = new ArrayList();
    List<Pokemon> psychic = new ArrayList();
    List<Pokemon> rock = new ArrayList();
    List<Pokemon> flying = new ArrayList();
    List<Pokemon> ghost = new ArrayList();
    List<Pokemon> ice = new ArrayList();
    List<Pokemon> dragon = new ArrayList();
    List<Pokemon> steel = new ArrayList();
    List<Pokemon> dark = new ArrayList();
    List<Pokemon> fairy = new ArrayList();

    public List<Pokemon> getRandom() {
        return random;
    }

    public void setRandom(List<Pokemon> random) {
        this.random = random;
    }

    public List<Pokemon> getGrass() {
        return grass;
    }

    public void setGrass(List<Pokemon> grass) {
        this.grass = grass;
    }

    public List<Pokemon> getFire() {
        return fire;
    }

    public void setFire(List<Pokemon> fire) {
        this.fire = fire;
    }

    public List<Pokemon> getWater() {
        return water;
    }

    public void setWater(List<Pokemon> water) {
        this.water = water;
    }

    public List<Pokemon> getBug() {
        return bug;
    }

    public void setBug(List<Pokemon> bug) {
        this.bug = bug;
    }

    public List<Pokemon> getNormal() {
        return normal;
    }

    public void setNormal(List<Pokemon> normal) {
        this.normal = normal;
    }

    public List<Pokemon> getPoison() {
        return poison;
    }

    public void setPoison(List<Pokemon> poison) {
        this.poison = poison;
    }

    public List<Pokemon> getElectric() {
        return electric;
    }

    public void setElectric(List<Pokemon> electric) {
        this.electric = electric;
    }

    public List<Pokemon> getGround() {
        return ground;
    }

    public void setGround(List<Pokemon> ground) {
        this.ground = ground;
    }

    public List<Pokemon> getFighting() {
        return fighting;
    }

    public void setFighting(List<Pokemon> fighting) {
        this.fighting = fighting;
    }

    public List<Pokemon> getPsychic() {
        return psychic;
    }

    public void setPsychic(List<Pokemon> psychic) {
        this.psychic = psychic;
    }

    public List<Pokemon> getRock() {
        return rock;
    }

    public void setRock(List<Pokemon> rock) {
        this.rock = rock;
    }

    public List<Pokemon> getFlying() {
        return flying;
    }

    public void setFlying(List<Pokemon> flying) {
        this.flying = flying;
    }

    public List<Pokemon> getGhost() {
        return ghost;
    }

    public void setGhost(List<Pokemon> ghost) {
        this.ghost = ghost;
    }

    public List<Pokemon> getIce() {
        return ice;
    }

    public void setIce(List<Pokemon> ice) {
        this.ice = ice;
    }

    public List<Pokemon> getDragon() {
        return dragon;
    }

    public void setDragon(List<Pokemon> dragon) {
        this.dragon = dragon;
    }

    public List<Pokemon> getSteel() {
        return steel;
    }

    public void setSteel(List<Pokemon> steel) {
        this.steel = steel;
    }

    public List<Pokemon> getDark() {
        return dark;
    }

    public void setDark(List<Pokemon> dark) {
        this.dark = dark;
    }

    public List<Pokemon> getFairy() {
        return fairy;
    }

    public void setFairy(List<Pokemon> fairy) {
        this.fairy = fairy;
    }
    
    
    List<Pokemon>pokemons = new ArrayList();
    Integer size_type;
    Integer size_abilities;

    public List<Pokemon> getPokemons() {
        return pokemons;
    }

    public void setPokemons(List<Pokemon> pokemons) {
        this.pokemons = pokemons;
    }
    
    
	private final String USER_AGENT = "Mozilla/5.0";
        
        

	public static void main(String[] args) throws Exception {
                            
		PokemonAPI http = new PokemonAPI();
		http.sendGet(); 
	}

	public void sendGet() throws Exception {
            
            for(int k=1;k<701;k++){

		String api = "https://pokeapi.co/api/v2/pokemon/";
		Integer num = k;
                if (num >= 802){
                    num = num + 199;
                }
                String num_string = num.toString();
                String url = api+num_string+"/";
                
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();

                StringBuffer response;
                try (BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()))) {
                    String inputLine;
                    response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                }
                String s = response.toString();
                
//CONVERTENDO STRING EM JSONOBJECT E JSONARRAY
            
    JSONParser parser = new JSONParser(); //converte a string para JSONArray ou JSONObject
        JSONObject objeto = (JSONObject) parser.parse(s);
            JSONArray array_tipo = (JSONArray) objeto.get("types"); //pegando o array do objeto maior
                JSONArray array_habilidades = (JSONArray) objeto.get("abilities");
    
                    size_type = array_tipo.size();
                    size_abilities = array_habilidades.size();
              
  
//Pegando Nome do Pokemon
  String nome_pokemon = (String) objeto.get("name");
 
//Pegando altura do Pokemon
  String altura_pokemon = objeto.get("height").toString();

 //Pegando peso do Pokemon
  String peso_pokemon = objeto.get("weight").toString();
 
 
    List<String> tipos = new ArrayList();
 //Pegando tipos do Pokemon
   for(Integer count=0; count < size_type; count++ ){
                JSONObject tipo = (JSONObject) array_tipo.get(count);
                tipo = (JSONObject) tipo.get("type");
                String tipo_pokemon = tipo.get("name").toString();
                tipos.add(count,tipo_pokemon);
   }

  List<String> habilidades = new ArrayList();
//Pegando habilidades do Pokemon
   for(Integer count=0; count < size_abilities; count++ ){
                JSONObject habilidade = (JSONObject) array_habilidades.get(count);
                habilidade = (JSONObject) habilidade.get("ability");
                String habilidade_pokemon = habilidade.get("name").toString();
                habilidades.add(count,habilidade_pokemon); 
   }
       Pokemon pokemon = new Pokemon(nome_pokemon,altura_pokemon,peso_pokemon,tipos,habilidades); 
       pokemons.add(k-1,pokemon);         
            }          
/*for(int j=0;j<10;j++){
    System.out.println("--------Pokemon---------");
    System.out.println("Nome: "+ pokemons.get(j).getNome());
    System.out.println("Altura: "+ pokemons.get(j).getAltura());
    System.out.println("Peso: "+ pokemons.get(j).getPeso());
    
    Integer lista_tipos = pokemons.get(j).getTipos().size();
    for(int l=0;l<lista_tipos;l++){
    System.out.println("Tipo "+ l +" : " + pokemons.get(j).getTipos().get(l));
    }
    Integer lista_habilidades = pokemons.get(j).getHabilidades().size();
    for(int l=0;l<lista_habilidades;l++){
    System.out.println("Habilidade "+ l +" : " + pokemons.get(j).getHabilidades().get(l));
    }
    System.out.println();

        }
*/
//SEPARANDO OS POKEMONS POR TIPOS
for(int i=1; i<700; i++){
            if(pokemons.get(i).getTipos().contains("grass")){
                grass.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("fire")){
                fire.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("water")){
                water.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("bug")){
                bug.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("normal")){
                normal.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("poison")){
                poison.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("electric")){
                electric.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("ground")){
               ground.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("fighting")){
                fighting.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("ghost")){
                ghost.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("flying")){
                flying.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("psychic")){
                psychic.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("rock")){
                rock.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("ice")){
                ice.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("dragon")){
                dragon.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("steel")){
                steel.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("dark")){
                dark.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTipos().contains("fairy")){
                fairy.add(pokemons.get(i));
            }
            }
/*
for(int i=0;i<bug.size();i++){
            System.out.println(bug.get(i).getNome());
}
for(int i=0;i<dark.size();i++){
            System.out.println(dark.get(i).getNome());
}
for(int i=0;i<dragon.size();i++){
            System.out.println(dragon.get(i).getNome());
}
for(int i=0;i<electric.size();i++){
            System.out.println(electric.get(i).getNome());
}
for(int i=0;i<fairy.size();i++){
            System.out.println(fairy.get(i).getNome());
}
for(int i=0;i<fighting.size();i++){
            System.out.println(fighting.get(i).getNome());
}
for(int i=0;i<fire.size();i++){
            System.out.println(fire.get(i).getNome());
}
for(int i=0;i<flying.size();i++){
            System.out.println(flying.get(i).getNome());
}
for(int i=0;i<ghost.size();i++){
            System.out.println(ghost.get(i).getNome());
}
for(int i=0;i<grass.size();i++){
            System.out.println(grass.get(i).getNome());
}
for(int i=0;i<ground.size();i++){
            System.out.println(ground.get(i).getNome());
}
for(int i=0;i<ice.size();i++){
            System.out.println(ice.get(i).getNome());
}
for(int i=0;i<normal.size();i++){
            System.out.println(normal.get(i).getNome());
}
for(int i=0;i<poison.size();i++){
            System.out.println(poison.get(i).getNome());
}
for(int i=0;i<psychic.size();i++){
            System.out.println(psychic.get(i).getNome());
}
for(int i=0;i<rock.size();i++){
            System.out.println(rock.get(i).getNome());
}
for(int i=0;i<steel.size();i++){
            System.out.println(steel.get(i).getNome());
}
for(int i=0;i<water.size();i++){
            System.out.println(water.get(i).getNome());
}
 */       }
   
}