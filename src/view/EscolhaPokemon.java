package view;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EscolhaPokemon extends javax.swing.JFrame {

    public EscolhaPokemon() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Tipos = new javax.swing.JPanel();
        imgPikaxu = new javax.swing.JLabel();
        jLTitulo = new javax.swing.JLabel();
        jLTipos = new javax.swing.JLabel();
        BFairy = new javax.swing.JButton();
        BSteel = new javax.swing.JButton();
        BDark = new javax.swing.JButton();
        BDragon = new javax.swing.JButton();
        BGhost = new javax.swing.JButton();
        BWater = new javax.swing.JButton();
        BRock = new javax.swing.JButton();
        BBug = new javax.swing.JButton();
        BPsychic = new javax.swing.JButton();
        BFlying = new javax.swing.JButton();
        BGround = new javax.swing.JButton();
        BFire = new javax.swing.JButton();
        BPoison = new javax.swing.JButton();
        BFighting = new javax.swing.JButton();
        BIce = new javax.swing.JButton();
        BEletric = new javax.swing.JButton();
        BGrass = new javax.swing.JButton();
        BNormal = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Tipos.setBackground(java.awt.Color.white);

        imgPikaxu.setBackground(java.awt.Color.darkGray);
        imgPikaxu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pikaxu.png"))); // NOI18N

        jLTitulo.setBackground(new java.awt.Color(253, 250, 247));
        jLTitulo.setFont(new java.awt.Font("Noto Sans CJK JP Regular", 1, 36)); // NOI18N
        jLTitulo.setText("MONTE SEU TIME");

        jLTipos.setText("ESCOLHA O TIPO");

        BFairy.setText("Fairy");
        BFairy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BFairyActionPerformed(evt);
            }
        });

        BSteel.setText("Steel");
        BSteel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BSteelActionPerformed(evt);
            }
        });

        BDark.setText("Dark");
        BDark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BDarkActionPerformed(evt);
            }
        });

        BDragon.setText("Dragon");
        BDragon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BDragonActionPerformed(evt);
            }
        });

        BGhost.setText("Ghost");
        BGhost.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BGhostActionPerformed(evt);
            }
        });

        BWater.setText("Water");
        BWater.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BWaterActionPerformed(evt);
            }
        });

        BRock.setText("Rock");
        BRock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BRockActionPerformed(evt);
            }
        });

        BBug.setText("Bug");
        BBug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BBugActionPerformed(evt);
            }
        });

        BPsychic.setText("Psychic");
        BPsychic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BPsychicActionPerformed(evt);
            }
        });

        BFlying.setText("Flying");
        BFlying.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BFlyingActionPerformed(evt);
            }
        });

        BGround.setText("Ground");
        BGround.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BGroundActionPerformed(evt);
            }
        });

        BFire.setText("Fire");
        BFire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BFireActionPerformed(evt);
            }
        });

        BPoison.setText("Poison");
        BPoison.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BPoisonActionPerformed(evt);
            }
        });

        BFighting.setText("Fighting");
        BFighting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BFightingActionPerformed(evt);
            }
        });

        BIce.setText("Ice");
        BIce.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BIceActionPerformed(evt);
            }
        });

        BEletric.setText("Eletric");
        BEletric.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BEletricActionPerformed(evt);
            }
        });

        BGrass.setText("Grass");
        BGrass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BGrassActionPerformed(evt);
            }
        });

        BNormal.setText("Normal");
        BNormal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BNormalActionPerformed(evt);
            }
        });

        jButton1.setText("Retornar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout TiposLayout = new javax.swing.GroupLayout(Tipos);
        Tipos.setLayout(TiposLayout);
        TiposLayout.setHorizontalGroup(
            TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TiposLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLTitulo)
                    .addComponent(jLTipos)
                    .addGroup(TiposLayout.createSequentialGroup()
                        .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(BFairy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BSteel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BDark, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BDragon, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                            .addComponent(BGhost, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BWater, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(25, 25, 25)
                        .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(BBug, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BPsychic, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                            .addComponent(BRock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BFlying, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BGround, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BFire, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(BNormal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BEletric, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BIce, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BFighting, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(BPoison, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BGrass, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(imgPikaxu, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        TiposLayout.setVerticalGroup(
            TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TiposLayout.createSequentialGroup()
                .addComponent(imgPikaxu)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1))
            .addGroup(TiposLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLTipos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BFairy)
                    .addComponent(BRock)
                    .addComponent(BPoison))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BSteel)
                    .addComponent(BBug)
                    .addComponent(BFighting))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BDark)
                    .addComponent(BPsychic)
                    .addComponent(BIce))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BDragon)
                    .addComponent(BFlying)
                    .addComponent(BEletric))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BGhost)
                    .addComponent(BGround)
                    .addComponent(BGrass))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BWater)
                    .addComponent(BFire)
                    .addComponent(BNormal))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tipos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tipos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    
    private void BFairyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BFairyActionPerformed
      TipoFairy telafairy;
        try {
            telafairy = new TipoFairy();
            telafairy.setVisible(true);  
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
                
    }//GEN-LAST:event_BFairyActionPerformed

    private void BGhostActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BGhostActionPerformed
         TipoGhost telaghost;
        try {
            telaghost = new TipoGhost();
            telaghost.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BGhostActionPerformed

    private void BWaterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BWaterActionPerformed
        TipoWater telawater;
        try {
            telawater = new TipoWater();
            telawater.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BWaterActionPerformed

    private void BBugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BBugActionPerformed
        TipoBug telabug;
        try {
            telabug = new TipoBug();
            telabug.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BBugActionPerformed

    private void BGroundActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BGroundActionPerformed
         TipoGround telaground;
        try {
            telaground = new TipoGround();
            telaground.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BGroundActionPerformed

    private void BFireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BFireActionPerformed
         TipoFire telafire;
        try {
            telafire = new TipoFire();
            telafire.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BFireActionPerformed

    private void BGrassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BGrassActionPerformed
      TipoGrass telagrass;
        try {
            telagrass = new TipoGrass();
            telagrass.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BGrassActionPerformed

    private void BPsychicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BPsychicActionPerformed
        TipoPsychic telapsychic;
        try {
            telapsychic = new TipoPsychic();
            telapsychic.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BPsychicActionPerformed

    private void BDragonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BDragonActionPerformed
         TipoDragon teladragon;
        try {
            teladragon = new TipoDragon();
            teladragon.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BDragonActionPerformed

    private void BPoisonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BPoisonActionPerformed
         TipoPoison telapoison;
        try {
            telapoison = new TipoPoison();
            telapoison.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BPoisonActionPerformed

    private void BSteelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BSteelActionPerformed
         TipoSteel telasteel;
        try {
            telasteel = new TipoSteel();
            telasteel.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BSteelActionPerformed

    private void BDarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BDarkActionPerformed
         TipoDark teladark;
        try {
            teladark = new TipoDark();
            teladark.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BDarkActionPerformed

    private void BRockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BRockActionPerformed
         TipoRock telarock;
        try {
            telarock = new TipoRock();
            telarock.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BRockActionPerformed

    private void BFlyingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BFlyingActionPerformed
         TipoFlying telaflying;
        try {
            telaflying = new TipoFlying();
            telaflying.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BFlyingActionPerformed

    private void BFightingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BFightingActionPerformed
         TipoFighting telafighting;
        try {
            telafighting = new TipoFighting();
            telafighting.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BFightingActionPerformed

    private void BIceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BIceActionPerformed
         TipoIce telaice;
        try {
            telaice = new TipoIce();
            telaice.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BIceActionPerformed

    private void BEletricActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BEletricActionPerformed
         TipoElectric telaelectric;
        try {
            telaelectric = new TipoElectric();
            telaelectric.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BEletricActionPerformed

    private void BNormalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BNormalActionPerformed
         TipoNormal telanormal;
        try {
            telanormal = new TipoNormal();
            telanormal.setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(EscolhaPokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
    }//GEN-LAST:event_BNormalActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new FormaAcesso().show(); 
        dispose(); 
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EscolhaPokemon.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EscolhaPokemon.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EscolhaPokemon.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EscolhaPokemon.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EscolhaPokemon().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BBug;
    private javax.swing.JButton BDark;
    private javax.swing.JButton BDragon;
    private javax.swing.JButton BEletric;
    private javax.swing.JButton BFairy;
    private javax.swing.JButton BFighting;
    private javax.swing.JButton BFire;
    private javax.swing.JButton BFlying;
    private javax.swing.JButton BGhost;
    private javax.swing.JButton BGrass;
    private javax.swing.JButton BGround;
    private javax.swing.JButton BIce;
    private javax.swing.JButton BNormal;
    private javax.swing.JButton BPoison;
    private javax.swing.JButton BPsychic;
    private javax.swing.JButton BRock;
    private javax.swing.JButton BSteel;
    private javax.swing.JButton BWater;
    private javax.swing.JPanel Tipos;
    private javax.swing.JLabel imgPikaxu;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLTipos;
    private javax.swing.JLabel jLTitulo;
    // End of variables declaration//GEN-END:variables
}
