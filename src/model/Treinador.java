package model;

public class Treinador {
    
    private String nomeTreinador;
    private String idade;
    private String sexo;

    public Treinador(){
        
    }
    public Treinador(String nome,String idade,String sexo){
        setNomeTreinador(nomeTreinador);
        setIdade(idade);
        setSexo(sexo);
        
    }
    
    public String getNomeTreinador() {
        return nomeTreinador;
    }

    public void setNomeTreinador(String nomeTreinador) {
        this.nomeTreinador = nomeTreinador;
    }
    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    
    
}
