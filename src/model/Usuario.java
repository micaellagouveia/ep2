package model;
public class Usuario extends Treinador{

private String login;
private String senha;

public Usuario(){
    
}
public Usuario(String nomeTreinador,String idade,String sexo,String login,String senha){
   setNomeTreinador(nomeTreinador);
   setIdade(idade);
   setSexo(sexo);
   setLogin(login);
   setSenha(senha);
}
    

public String getLogin() {
    return login;
}

public void setLogin(String login) {
    this.login = login;
}
public String getSenha() {
    return senha;
}

public void setSenha(String senha) {
    this.senha = senha;
}


}