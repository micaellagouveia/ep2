
package model;

import java.util.List;

public class Pokemon {

    private String nome;
    private String altura;
    private String peso;
    private List<String> habilidades;
    private List<String> tipos;

    public Pokemon(String nome,String altura,String peso,List tipos,List habilidades){
    setNome(nome);
    setAltura(altura);
    setPeso(peso);
    setTipos(tipos);
    setHabilidades(habilidades);
    }

    public List<String> getHabilidades() {
        return habilidades;
    }

    public void setHabilidades(List<String> habilidades) {
        this.habilidades = habilidades;
    }

    public List<String> getTipos() {
        return tipos;
    }

    public void setTipos(List<String> tipos) {
        this.tipos = tipos;
    }
    
    public String getAltura(){
        return altura;
    }
    public void setAltura(String altura){
        this.altura = altura;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

}



    
    
    

