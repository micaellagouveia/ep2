# Exercício Programado 2

Utilizei a IDE Netbeans.

##Funcionamento da Pokedex

1º) Executa a jFrame "Tela inicial", nela se faz o cadastro do treinador.

2º) Escolha como quer filtrar os pokemons(por tipos ou por nomes)

3º) Se for por NOME, aparecerá uma lista com todos os pokemons 

4º) Se for por TIPOS, você escolherá o tipo, e então aparecerá uma lista com os pokemons do tipo escolhido

##Ressalvas
Não consegui colocar os atributos de cada pokemon na interface, mas peguei seus nomes, tipos, habilidades, pesos e alturas na API. Tem o print dessa requisição no PokemonAPI comentado, caso precise, so descomentar e verá que pego seus atributos.Tem também comentado, a separação dos tipos dos pokemons. A dificuldade foi passar para a interface, quando clico em algum pokemon(a ação de clicar com o mouse não funciona).

Não sei se é por causa da internet, mas fazer a requisição da API demora muito, mas não em fazer a reposição usando o PokemonAPI, mas sim quando usa-se a interface.
Executando somente o PokemonAPI, a requisição demora em torno de 5 minutos para ler os pokemons, mas quando usa a interface ele geralmente demora mais, então por favor sejam pacientes hahaha. E muitas vezes ele abre a interface, mas n carrega todos os pokemons imediatamente, então tem que esperar o programa rodar por completo para que apareçam todos os pokemons, seja do filtro por nome, ou por tipo.

No meu pacote control, faço a requisição da API, armazeno os dados dos pokemons e os separo em tipos
No meu pacote model, possui as classes Pokemon, Treinador e Usuário, sendo que Usuário herda de Treinador.
O diagrama de classes também está no projeto, na pasta DiagramaDeClasses.

A jFrame DetalhesPokemons seria utilizada para mostrar os atributos dos pokemons clicados, mas como disse, a ação de apertar com o mouse não está funcionando.

Com isso, não consegui fazer o relatório final, da relação do treinador com seus pokemons.

jFrame Tela Inicial -> primeira tela, faz-se o cadastro

jFrame Login -> acessa com o login e senha

jFrame FormaAcesso -> filtro por tipos e nome

jFrame TodosPokemons -> lista que uso quando clico em nome, mostrando uma lista com todos os pokemons

jFrame EscolhaPokemon -> quando aperto em tipos, essa janela se abre, e me mostra todos os tipos de pokemons para escolher

jFrame de todos os tipos -> cada jFrame mostra uma lista de pokemons de cada tipo.
